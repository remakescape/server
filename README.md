# RemakeScape Server
[![Build Pipeline Status](https://img.shields.io/gitlab/pipeline/remakescape/server.svg)](https://gitlab.com/remakescape/server/pipelines)
[![Docker Image](https://img.shields.io/badge/docker%20build-automated-blue.svg)](https://gitlab.com/remakescape/server/container_registry)
[![Join Discord Server](https://img.shields.io/badge/discord-join-738adb.svg)](https://discord.gg/XYYuKn2)
[![Built with RSMod](https://img.shields.io/badge/built%20with-rsmod-ff007f.svg)](https://github.com/Tomm0017/rsmod)

RemakeScape is an open-source running OSRS private server that aims to re-create the current content of the game as of 1/1/2019. The project aims to provide a newer stable base build on RSMod for anyone looking to make a RSPS to start from. Additionally I will be running servers updated with the master branch for anyone to play on. The project was started by Kyle Escobar as a hobby open to anyone and is in no way money driven.

## Live Server
We are planning to run a live server that gets updated with the lasted master branch daily at midnight. Learn more on how to join the action at [https://remakescape.com](https://remakescape.com)

## Installation
If you have any troubles getting RemakeScape setup feel free to hope over to our discord and ask for help.

Before we setup the project you must first download the OSRS rev180 cache. This can be downloaded from [https://archive.runestats.com/osrs/2019-05-23-rev180.tar.gz](https://archive.runestats.com/osrs/2019-05-23-rev180.tar.gz)

You will need the `cache` and `xteas.json` from this archive.

### Docker
Installation Wiki coming soon.

### From Source
Installation Wiki coming soon.

### Developers
Developer Wiki coming soon.


## Features
You can always check the milestone and issues for status on specific issue. Some milestone issues may not be listed or updated here



## Join Our Community
We are aiming to be a community driven project. If you are interested in providing feedback to features or even contributing code to project make sure you join our [discord](https://discord.gg/fkldsjf).

## Donations and VIP
Being this project is community driven and open-source, no one is in it for any profit. That being said servers cost money and so does time. If you want to show your support and buy us a cup of coffee, we will have a patreon which will give you the VIP rank and color both in game and in Discord. Keep in mind that this does nothing other than make your name purple in game to identify you.