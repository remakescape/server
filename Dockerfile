FROM gradle:jdk8

ENV dir /app

WORKDIR ${dir}

ADD . .

RUN gradle build -x test

# Get the distribution and unzip it
RUN mkdir dist
RUN mv game/build/distributions/game-shadow-*.zip /app/
RUN unzip -q -o game-shadow-*.zip
RUN rm -rf game-shadow-*.zip
RUN cp -R game-shadow-*/* dist/
RUN rm -rf game-shadow-*

EXPOSE 43594:43594/tcp

ENTRYPOINT /app/dist/bin/game