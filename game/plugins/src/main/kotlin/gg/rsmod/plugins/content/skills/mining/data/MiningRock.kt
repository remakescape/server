package gg.rsmod.plugins.content.skills.mining.data

enum class MiningRock(val ore: MiningOre, val respawnTicks: Int, val rocks: IntArray, val emptyRocks: IntArray) {
    COPPER(MiningOre.COPPER, 5, intArrayOf(10079, 11161, 10943), intArrayOf(10081, 11391, 11390)),
    TIN(MiningOre.TIN, 5, intArrayOf(10080, 11360, 11361), intArrayOf(10081, 11390, 11391));

    fun getEmptyObj(obj: Int): Int {
        val defs: HashMap<Int, Int> = hashMapOf()
        for(i in (0..(rocks.size-1))) {
            defs.put(rocks[i], emptyRocks[i])
        }

        return defs.get(obj)!!
    }

    companion object {
        val values = enumValues<MiningRock>()
    }
}