package gg.rsmod.plugins.content.items

val UNTRADABLES = intArrayOf(
        Items.COINS_995
)

UNTRADABLES.forEach { item ->
    world.definitions.get(ItemDef::class.java, item).tradeable = false
}