package gg.rsmod.plugins.content.skills.mining

import gg.rsmod.game.fs.DefinitionSet
import gg.rsmod.game.fs.def.ItemDef
import gg.rsmod.game.model.entity.DynamicObject
import gg.rsmod.game.model.entity.Player
import gg.rsmod.game.model.queue.QueueTask
import gg.rsmod.plugins.api.EquipmentType
import gg.rsmod.plugins.api.Skills
import gg.rsmod.plugins.api.cfg.Objs
import gg.rsmod.plugins.api.ext.*
import gg.rsmod.plugins.content.skills.mining.data.MiningOre
import gg.rsmod.plugins.content.skills.mining.data.MiningPickaxe
import gg.rsmod.plugins.content.skills.mining.data.MiningRock

class Mining(val defs: DefinitionSet) {

    val EMPTY_ROCKS = Objs.ROCKS_11391

    val pickaxeDefs = MiningPickaxe.values.associate { it.pickaxe to it }

    val oreNames = MiningOre.values.associate { it.ore to defs.get(ItemDef::class.java, it.ore).name.toLowerCase() }

    val rocks = MiningRock.values

    suspend fun mine(it: QueueTask, rock: MiningRock) {
        val player = it.player

        if(!canMine(player, rock)) {
            player.animate(-1)
            return
        }

        val pickaxe = getBestPickaxe(player)!!

        val rockObj = player.getInteractingGameObj()

        player.filterableMessage("You swing your pick at the rock.")

        while(true) {
            it.wait(2)
            player.animate(pickaxe.animation)

            if(!canMine(player,rock)) {
                player.animate(-1)
                break
            }

            val level = player.getSkills().getCurrentLevel(Skills.MINING)
            val minChance = 60 + (60 * (pickaxe.bonus*100))
            if(level.interpolate(minChance = minChance.toInt(), maxChance = 160, minLvl = rock.ore.level, maxLvl = 99, cap = 255)) {
                player.inventory.add(rock.ore.ore)
                if(rock.ore.xp > 0.0) {
                    player.addXp(Skills.MINING, rock.ore.xp)
                }

                player.world.queue {
                    val emptyRock = DynamicObject(rockObj, rock.getEmptyObj(rockObj.id))
                    val respawnRock = DynamicObject(rockObj)
                    player.world.remove(rockObj)
                    player.world.spawn(emptyRock)
                    wait(rock.respawnTicks)
                    player.world.remove(emptyRock)
                    player.world.spawn(respawnRock)
                }

                player.animate(-1)

                player.filterableMessage("You manage to mine some ${oreNames[rock.ore.ore]}.")

                break
            }

            it.wait(2)
        }
    }

    suspend fun prospect(it: QueueTask, rock: MiningRock) {
        val player = it.player
        player.filterableMessage("You examine the rock for ores...")
        it.wait(5)
        player.filterableMessage("This rock contains ${oreNames[rock.ore.ore]}.")
    }

    private fun canMine(player: Player, rock: MiningRock): Boolean {
        if(getBestPickaxe(player) == null) {
            player.queue { messageBox("You need a pickaxe to mine this rock. You do not have a pickaxe<br>which you have the Mining level to use.")}
            return false
        }

        if(player.getSkills().getCurrentLevel(Skills.MINING) < rock.ore.level) {
            player.queue { messageBox("You need a Mining level of ${rock.ore.level} to mine this rock.") }
            return false
        }

        if(player.inventory.freeSlotCount == 0) {
            player.queue { messageBox("Your inventory is too full to hold any more ${oreNames[rock.ore.ore]}.")}
            return false
        }

        return true
    }

    private fun getBestPickaxe(player: Player): MiningPickaxe? {
        var best: MiningPickaxe? = null
        for(i in (0..27)) {
            val invItem = player.inventory.get(i)
            if (invItem != null) {
                if(pickaxeDefs[invItem.id] != null) {
                    if(best == null || best.bonus < pickaxeDefs[invItem.id]!!.bonus) {
                        best = pickaxeDefs[invItem.id]
                    }
                }
            }
        }

        val equiped = player.getEquipment(EquipmentType.WEAPON)
        if(equiped != null) {
            if(pickaxeDefs[equiped.id] != null) {
                if(best == null || best.bonus < pickaxeDefs[equiped.id]!!.bonus) {
                    best = pickaxeDefs[equiped.id]
                }
            }
        }
        return best
    }
}