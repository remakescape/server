package gg.rsmod.plugins.api.modules.instance

import gg.rsmod.game.model.Tile
import gg.rsmod.game.model.World
import gg.rsmod.game.model.entity.Player
import gg.rsmod.game.model.instance.InstancedChunkSet
import gg.rsmod.game.model.instance.InstancedMap
import gg.rsmod.game.model.instance.InstancedMapConfiguration

class InstanceBuilder(startTile: Tile, endTile: Tile, instanceHeight: Int) {
    private var startTile = Tile(x = (startTile.x - (startTile.x % 8)), z = (startTile.z - (startTile.z % 8)), height = startTile.height)
    private var endTile = Tile(x = (endTile.x - (endTile.x % 8)), z = (endTile.z - (endTile.z % 8)), height = endTile.height)

    private var instance: InstancedChunkSet? = null

    private val instanceHeight = instanceHeight

    private var instanceLocation: Tile? = null

    private var instanceMap: InstancedMap? = null

    fun generate(world: World, config: InstancedMapConfiguration): InstancedMap? {
        val chunks = InstancedChunkSet.Builder()

        val cx = (endTile.x - startTile.x) / 8
        val cz = (endTile.z - startTile.z) / 8

        for(z in (0..(cz-1))) {
            for(x in (0..(cx-1))) {
                chunks.set(
                        chunkX = x,
                        chunkZ = z,
                        height = 0,
                        rot = 0,
                        copy = Tile(x = (startTile.x + (x*8)), z = (startTile.z + (z*8)), height = 0)
                )
            }
        }

        instance = chunks.build()

        world.instanceAllocator.allocate(world, instance!!, config)?.let { map ->
            val loc = map.area.bottomLeft
            instanceLocation = loc
            instanceMap = map
            return map
        }
        return null
    }

    fun getEntryTile(): Tile? { return this.instanceLocation }

    fun translateTile(tile: Tile): Tile? {
        return Tile(x = (instanceLocation!!.x + (tile.x - startTile.x)), z = (instanceLocation!!.z + (tile.z - startTile.z)))
    }
}