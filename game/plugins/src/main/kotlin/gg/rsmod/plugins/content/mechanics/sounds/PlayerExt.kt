package gg.rsmod.plugins.content.mechanics.sounds

import gg.rsmod.game.model.entity.Player
import gg.rsmod.plugins.api.ext.*
import gg.rsmod.plugins.content.mechanics.sounds.util.BitUtils

fun Player.unlockSong(song: Song) {
    var value = 0

    try {
        value = this.getVarp(song.varpNumber)
    } catch(e: Exception) {
        value = 0
    }

    this.setVarp(song.varpNumber, BitUtils.toggleBit(value, song.bitIndex))
}

fun Player.hasSongUnlocked(song: Song): Boolean {
    try {
        return (BitUtils.testBit(this.getVarp(song.varpNumber), song.bitIndex))
    } catch(e: Exception) {
        return false
    }
}

fun Player.playSong(song: Song) {
    if(!this.hasSongUnlocked(song)) {
        this.unlockSong(song)
    }
    this.playSong(song.id)
    this.setComponentText(interfaceId = 239, component = 6, text = song.songName)
}