package gg.rsmod.plugins.content.npcs.man

import gg.rsmod.game.model.npcdrops.NPCDropEntry

val MAN = intArrayOf(
        Npcs.MAN_3078,
        Npcs.MAN_3079,
        Npcs.MAN_3080,
        Npcs.MAN_3081,
        Npcs.MAN_3082
)

MAN.forEach { npc ->
    set_combat_def(npc) {
        configs {
            respawnDelay = 60
            attackSpeed = 4
        }

        stats {
            hitpoints = 7
            attack = 1
            strength = 1
            defence = 1
            magic = 1
            ranged = 1
        }

        bonuses {
            attackStab = 0
            attackSlash = 0
            attackCrush = 0
            attackMagic = 0
            attackRanged = 0

            defenceStab = -21
            defenceSlash = -21
            defenceCrush = -21
            defenceMagic = -21
            defenceRanged = -21

            attackBonus = 0
            strengthBonus = 0
            rangedStrengthBonus = 0
            magicDamageBonus = 0
        }

        anims {
            block = 425
            death = 836
            attack = 422
        }

        sounds {
            hurt = 513
            death = 512
            attack = 2566
        }
    }
}