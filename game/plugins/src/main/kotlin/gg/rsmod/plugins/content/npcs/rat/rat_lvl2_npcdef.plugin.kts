package gg.rsmod.plugins.content.npcs.rat

import gg.rsmod.game.model.npcdrops.NPCDropEntry

val RAT = intArrayOf(
        Npcs.RAT_2854,
        Npcs.RAT
)

RAT.forEach { npc ->
    set_combat_def(npc) {
        configs {
            attackSpeed = 4
            respawnDelay = 30
        }

        stats {
            hitpoints = 2
            attack = 1
            strength = 0
            defence = 0
            magic = 0
            ranged = 0
        }

        bonuses {
            attackStab = 0
            attackSlash = 0
            attackCrush = 0
            attackMagic = 0
            attackRanged = 0
            defenceStab = 0
            defenceSlash = 0
            defenceCrush = 0
            defenceMagic = 0
            defenceRanged = 0

            attackBonus = 0
            strengthBonus = 0
            rangedStrengthBonus = 0
            magicDamageBonus = 0
        }

        anims {
            attack = 2705
            death = 2707
            block = 2706
        }

        slayerData {
            levelRequirement = 1
            xp = 2.0
        }

        species {
        }

        sounds {
            attack = 713
            hurt = 713
            death = 711
        }
    }

    set_drop_table(npc) {
        config {
            table_rolls = 1
        }

        drop_table {
            always_table = arrayOf(
                    NPCDropEntry(Items.BONES, 1, 1)
            )
        }
    }
}