package gg.rsmod.plugins.content.mechanics.sounds

val songs = Song.values()

songs.forEach { song ->
    if(song.regionId > 0) {
        on_enter_region(song.regionId) {
            player.playSong(song)
        }
    }
}

on_interface_open(interfaceId = 239) {
    player.setInterfaceEvents(interfaceId = 239, component = 3, range = 0..599, setting = 6)
    player.setComponentText(interfaceId = 239, component = 6, text = "AUTO")
}