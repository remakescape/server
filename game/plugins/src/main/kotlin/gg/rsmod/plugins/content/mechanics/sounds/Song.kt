package gg.rsmod.plugins.content.mechanics.sounds

enum class Song(val songName: String, val id: Int, val regionId: Int, val varpNumber: Int, val bitIndex: Int) {
    NONE("", -1, 0, 0, 0),
    HARMONY("Harmony", 76, 12850, 21, 26),
    AUTUMN_VOYAGE("Autumn Voyage", 2, 12851, 20, 17),
    DREAM("Dream", 327, 12594, 21, 3),
    FLUTE_SALAD("Flute Salad", 163, 12595, 21, 15),
    YESTERYEAR("Yesteryear", 145, 12849, 25, 1)
}