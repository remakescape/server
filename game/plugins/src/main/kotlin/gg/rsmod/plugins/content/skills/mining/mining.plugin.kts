package gg.rsmod.plugins.content.skills.mining

import gg.rsmod.plugins.content.skills.mining.data.MiningRock

val mining = Mining(world.definitions)

val rocks = MiningRock.values

rocks.forEach { rock ->
    rock.rocks.forEach { obj ->
        on_obj_option(obj, "mine") {
            player.queue { mining.mine(this, rock) }
        }

        on_obj_option(obj, "prospect") {
            player.queue { mining.prospect(this, rock) }
        }


    }
}