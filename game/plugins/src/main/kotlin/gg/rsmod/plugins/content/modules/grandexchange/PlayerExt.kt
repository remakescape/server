package gg.rsmod.plugins.content.modules.grandexchange

import gg.rsmod.game.model.entity.Player
import gg.rsmod.game.model.queue.TaskPriority
import gg.rsmod.plugins.api.ext.*
import sun.audio.AudioPlayer.player

fun Player.openGrandExchange() {
    GEInterface(this).open()
}

fun Player.closeGrandExchange() {
    GEInterface(this).close()
}

/**
 * Triggered when the player clicks buy / sell within the grand exchange.
 * @param type      0 = buy 1 = sell
 */
fun Player.openGrandExchangeOffer(slot: Int, type: Int) {
    GEInterface(this).openOffer(slot, type)
}

fun Player.closeGrandExchangeOffer() {
    GEInterface(this).closeOffer(this.getVarbit(GrandExchange.VARBIT_OFFER_CREATION_TYPE))
}

fun Player.reselectGrandExchangeOfferItem() {
    if(this.getVarbit(GrandExchange.VARBIT_OFFER_CREATION_TYPE) == 0) {
        GEInterface(this).clearOfferItem()
        this.queue(TaskPriority.WEAK) { GEInterface(this.player).buyItemSearch(this) }
    } else {
        GEInterface(this).clearOfferItem()
    }
}

fun Player.changeGEQuantity(delta: Int, max: Int = -1) {
    var quantity = getVarp(GrandExchange.VARP_QUANITY)
    val price = getVarp(GrandExchange.VARP_PRICE)

    var skipAdd = false

    if(quantity == 0 || price == 0) {
        return
    }

    when(getVarbit(GrandExchange.VARBIT_OFFER_CREATION_TYPE)) {
        0 -> {
            if(quantity+delta <= 0) { return }
        }

        1 -> {
            if(quantity+delta == Int.MIN_VALUE || quantity+delta >= 0) { return }
            if((max > -1 && quantity+delta > Int.MIN_VALUE+max) || delta == 1000) {
                quantity = Int.MIN_VALUE+max
                skipAdd = true
            }
        }
    }

    if(!skipAdd) { quantity+=delta }

    setVarbit(GrandExchange.VARBIT_QUANITY, quantity)
    setVarp(GrandExchange.VARP_QUANITY, quantity)
}

fun Player.changeGEPrice(delta: Int) {
    val current_quantity = getVarp(GrandExchange.VARP_QUANITY)
    var current_price = getVarp(GrandExchange.VARP_PRICE)

    if(current_quantity == 0 || current_price == 0) {
        return
    }

    if(current_price+delta <= 0) {
        return
    }

    if(current_price > 20 && current_price%20 != 0) {
        if(delta > 0) { current_price-- }
        else { current_price++ }
    }

    current_price += delta
    setVarp(GrandExchange.VARP_PRICE, current_price)
    setVarbit(GrandExchange.VARBIT_PRICE, current_price)
}