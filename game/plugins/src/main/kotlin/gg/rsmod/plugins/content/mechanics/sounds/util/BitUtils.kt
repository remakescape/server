package gg.rsmod.plugins.content.mechanics.sounds.util

object BitUtils {
    fun testBit(value: Int, bit: Int): Boolean {
        return (value and 1 shl bit) != 0
    }

    fun clearBit(value: Int, bit: Int): Int {
        return value and (1 shl bit).inv()
    }

    fun setBit(value: Int, bit: Int): Int {
        return value or (1 shl bit)
    }

    fun toggleBit(value: Int, bit: Int): Int {
        return value xor (1 shl bit)
    }
}