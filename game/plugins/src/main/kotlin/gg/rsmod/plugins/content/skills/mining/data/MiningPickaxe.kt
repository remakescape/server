package gg.rsmod.plugins.content.skills.mining.data

import gg.rsmod.plugins.api.cfg.Items

enum class MiningPickaxe(val pickaxe: Int, val minLevel: Int, val bonus: Double, val animation: Int) {
    BRONZE(Items.BRONZE_PICKAXE, 1, 0.0, 625),
    IRON(Items.IRON_PICKAXE,  1, 0.5, 626),
    STEEL(Items.STEEL_PICKAXE,  6, 0.7, 627),
    BLACK(Items.BLACK_PICKAXE, 11, 0.8, 628),
    MITHRIL(Items.MITHRIL_PICKAXE,  21, 1.0, 629),
    ADAMANT(Items.ADAMANT_PICKAXE,  31, 2.0, 630),
    RUNE(Items.RUNE_PICKAXE,  41,3.5, 624),
    DRAGON(Items.DRAGON_PICKAXE, 61, 5.5, 7139),
    INFERNAL(Items.INFERNAL_PICKAXE,  61, 6.0, 624);

    companion object {
        val values = enumValues<MiningPickaxe>()
    }
}