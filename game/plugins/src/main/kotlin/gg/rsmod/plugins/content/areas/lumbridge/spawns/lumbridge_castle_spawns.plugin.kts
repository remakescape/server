package gg.rsmod.plugins.content.areas.lumbridge.spawns

// Rats
spawn_npc(Npcs.RAT_2854, 3223, 3223, 0, 5)
spawn_npc(Npcs.RAT_2854, 3229, 3223, 0, 5)
spawn_npc(Npcs.RAT_2854, 3229,3214, 0, 5)
spawn_npc(Npcs.RAT_2854, 3214,3207,0,3)
spawn_npc(Npcs.RAT_2854, 3214,3205,0,3)
spawn_npc(Npcs.RAT_2854,3208,3205,0,3)
spawn_npc(Npcs.RAT_2854,3208,3207,0,3)
spawn_npc(Npcs.RAT_2854, 3235, 3225, 0, 5)

// Hans
spawn_npc(3077, 3219,3223,0, 0, Direction.EAST)

// Gee
spawn_npc(3261, 3222,3229,0, 15)

spawn_npc(Npcs.LUMBRIDGE_GUIDE_306, 3238,3220,0, 0, Direction.WEST)
spawn_npc(Npcs.PRAYER_TUTOR, 3244,3212,0,3)
spawn_npc(Npcs.FATHER_AERECK, 3243,3206,0,3)
spawn_npc(Npcs.HATIUS_COSAINTUS, 3233,3215,0,1)
spawn_npc(Npcs.ADAM, 3229,3228,0,1)
spawn_npc(Npcs.DOOMSAYER, 3232,3232,0, 0, Direction.SOUTH)

// Man
spawn_npc(Npcs.MAN_3078, 3217,3219,0,5)
spawn_npc(Npcs.MAN_3079, 3214,3227,0 , 5)
spawn_npc(Npcs.MAN_3080, 3222, 3213, 0, 5)
spawn_npc(Npcs.WOMAN_3083, 3220, 3229, 0, 5)
spawn_npc(Npcs.MAN_3081, 3234,3219, 0, 5)
spawn_npc(Npcs.MAN_3078, 3235, 3201, 0, 5)
spawn_npc(Npcs.WOMAN_3084, 3238,3206,0, 5)
spawn_npc(Npcs.WOMAN_3085, 3230,3207,0, 5)
spawn_npc(Npcs.WOMAN_3083, 3217, 3206, 0, 5)
spawn_npc(Npcs.WOMAN_3085, 3241,3210,0, 5)

// Goblin
spawn_npc(Npcs.GOBLIN_3029, 3234, 3234, 0, 1)
spawn_npc(Npcs.GOBLIN_3031, 3237, 3228, 0, 1)

// Aurther the clue hunter
spawn_npc(Npcs.ARTHUR_THE_CLUE_HUNTER, 3229, 3236, 0, 0, Direction.NORTH)

spawn_npc(Npcs.MAN_7547, 3229, 3239, 0, 3)
spawn_npc(Npcs.BARTENDER_7546, 3232, 3241, 0, 0, Direction.WEST)
spawn_npc(Npcs.VEOS_8484, 3228, 3242, 0, 0, Direction.SOUTH)

// Combat tutors
spawn_npc(Npcs.MELEE_COMBAT_TUTOR, 3220, 3238, 0, 1)
spawn_npc(Npcs.RANGED_COMBAT_TUTOR, 3218, 3238, 0, 1)
spawn_npc(Npcs.MAGIC_COMBAT_TUTOR, 3216, 3238,0, 1)

// wOODCUTTING Tutor
spawn_npc(Npcs.WOODSMAN_TUTOR, 3228,3246,0,1)

spawn_npc(Npcs.MAN_3080, 3220, 3242, 0, 7)
spawn_npc(Npcs.MAN_3078, 3223, 3246, 0, 7)