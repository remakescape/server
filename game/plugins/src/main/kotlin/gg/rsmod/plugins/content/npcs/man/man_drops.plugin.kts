package gg.rsmod.plugins.content.npcs.man

import gg.rsmod.game.model.npcdrops.NPCDropEntry

val MAN = intArrayOf(
        Npcs.MAN_3078,
        Npcs.MAN_3079,
        Npcs.MAN_3080,
        Npcs.MAN_3081,
        Npcs.MAN_3082
)

MAN.forEach { npc ->

    set_drop_table(npc) {
        config {
            table_rolls = 1
        }

        drop_table {
            always_table = arrayOf(
                    NPCDropEntry(Items.BONES, 1, 1)
            )

            common_table = arrayOf(
                    NPCDropEntry(Items.GRIMY_GUAM_LEAF, 1,1),
                    NPCDropEntry(Items.GRIMY_MARRENTILL, 1,1),
                    NPCDropEntry(Items.GRIMY_TARROMIN, 1,1),
                    NPCDropEntry(Items.BRONZE_BOLTS, 2, 12),
                    NPCDropEntry(Items.BRONZE_ARROW, 7, 7),
                    NPCDropEntry(Items.COINS_995, 3,3),
                    NPCDropEntry(Items.COINS_995, 5, 5),
                    NPCDropEntry(Items.COINS_995, 15,15),
                    NPCDropEntry(Items.COINS_995, 25,25),
                    NPCDropEntry(Items.FISHING_BAIT, 1,1)
            )

            uncommon_table = arrayOf(
                    NPCDropEntry(Items.EARTH_RUNE, 4, 4),
                    NPCDropEntry(Items.FIRE_RUNE, 6,6),
                    NPCDropEntry(Items.MIND_RUNE, 9,9),
                    NPCDropEntry(Items.CHAOS_RUNE, 2, 2),
                    NPCDropEntry(Items.GRIMY_HARRALANDER, 1, 1),
                    NPCDropEntry(Items.GRIMY_RANARR_WEED, 1, 1),
                    NPCDropEntry(Items.GRIMY_KWUARM, 1, 1),
                    NPCDropEntry(Items.GRIMY_IRIT_LEAF, 1, 1),
                    NPCDropEntry(Items.GRIMY_AVANTOE, 1, 1),
                    NPCDropEntry(Items.IRON_DAGGER, 1, 1),
                    NPCDropEntry(Items.BRONZE_MED_HELM, 1,1),
                    NPCDropEntry(Items.EARTH_TALISMAN, 1, 1)
            )

            rare_table = arrayOf(
                    NPCDropEntry(Items.GRIMY_CADANTINE, 1, 1),
                    NPCDropEntry(Items.GRIMY_DWARF_WEED, 1, 1),
                    NPCDropEntry(Items.GRIMY_LANTADYME, 1, 1),
                    NPCDropEntry(Items.CABBAGE, 1,1),
                    NPCDropEntry(Items.COPPER_ORE, 1,1)
            )
        }
    }
}