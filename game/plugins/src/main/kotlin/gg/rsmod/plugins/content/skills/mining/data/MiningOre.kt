package gg.rsmod.plugins.content.skills.mining.data

import gg.rsmod.plugins.api.cfg.Items

enum class MiningOre(val ore: Int, val level: Int, val xp: Double) {
    CLAY(Items.CLAY, 1, 5.0),
    COPPER(Items.COPPER_ORE, 1, 17.5),
    TIN(Items.TIN_ORE, 1, 17.5),
    LIMESTONE(Items.LIMESTONE, 10, 26.5),
    BLURITE(Items.BLURITE_ORE, 10, 17.5),
    IRON(Items.IRON_ORE, 15, 35.0),
    ELEMENTAL(Items.ELEMENTAL_ORE, 20, 0.0),
    DAEYALT(Items.DAEYALT_ORE, 20, 17.0),
    SILVER(Items.SILVER_ORE, 20, 40.0),
    VOLCANIC_ASH(Items.VOLCANIC_ASH, 22, 10.0),
    COAL(Items.COAL, 30, 50.0),
    GOLD(Items.GOLD_ORE, 40, 65.0),
    VOLCANIC_SULPHUR(Items.VOLCANIC_SULPHUR, 42, 25.0),
    MITHRIL(Items.MITHRIL_ORE, 55, 80.0),
    ADAMANTITE(Items.ADAMANTITE_ORE, 70, 95.0),
    RUNITE(Items.RUNITE_ORE, 85, 125.0);

    companion object {
        val values = enumValues<MiningOre>()
    }
}