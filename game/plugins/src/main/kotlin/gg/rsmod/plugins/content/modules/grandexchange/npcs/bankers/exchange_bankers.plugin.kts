package gg.rsmod.plugins.content.modules.grandexchange.npcs.bankers

import gg.rsmod.plugins.content.inter.bank.openBank

val BANKERS = intArrayOf(Npcs.BANKER_2117, Npcs.BANKER_2118, Npcs.BANKER_2119, Npcs.BANKER_8682)

BANKERS.forEach { npc ->
    on_npc_option(npc, "bank", 1) {
        player.openBank()
    }
}