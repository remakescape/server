package gg.rsmod.plugins.content.npcs.tutors.melee_tutor

import khttp.options

on_npc_option(Npcs.MELEE_COMBAT_TUTOR, "talk-to") {
    player.queue {
       initConvo(this)
    }
}

suspend fun initConvo(it: QueueTask) {
    val player = it.player
    when(it.options("Can you tell me about different weapon types I can use?", "Please tell me about skillcapes.", "Bye.", title = "Select an Option")) {
        1 -> {
            player.queue { chatAboutWeaponTypes(this) }
        }
        2 -> {
            player.queue { chatAboutSkillcapes(this) }
        }
        3 -> {
            it.chatPlayer("Bye.")
            it.player.closeInputDialog()
        }
    }
}

suspend fun chatAboutWeaponTypes(it: QueueTask) {
    it.chatPlayer(animation = 555, message = "Can you tell me about different weapon types I can<br>use?")
    it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 570, message = "Well let me see now...There are stabbing type weapons<br>such as daggers, then you have swords which are<br>slashing, maces that have great crushing abilities, battle<br>axes which are powerful and spears which can be good")
    it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 567, message = "for Defence and many forms of Attack.")
    it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 591, message = "It depends a lot on how you want to fight. Experiment<br>and find out what is best for you. Never be scared to<br>try out a new weapon; you never know, you might like<br>it! Why, I tried all of them for a while and settled on")
    it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 588, message = "this rather good sword!")

    when(it.options("I'd like a training sword and shield.", "Bye.")) {
        1 -> {
            it.chatPlayer(animation = 567, message = "I'd like a training sword and shield.")
            it.player.queue { giveTrainingGear(this) }
        }
        2 -> {
            it.chatPlayer("Bye.")
            it.player.closeInputDialog()
        }
    }
}

suspend fun chatAboutSkillcapes(it: QueueTask) {
    it.chatPlayer(animation = 554, message = "Please tell me about skillcapes.")
    it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 591, message = "Of course. Skillcapes are a symbol of achievement. Only<br>people who have mastered a skill and reached level 99<br>can get their hands on them and gain the benefits they<br>carry.")
    it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 590, message = "The Cape of Defence will act as ring of life, saving you<br>from combat if your hitpoints become low. Is there<br>something else I can help you with, perhaps?")
    it.player.queue { initConvo(this) }
}

suspend fun giveTrainingGear(it: QueueTask) {
    val player = it.player

    var hasSword = false
    var hasShield = false

    if(player.inventory.contains(Items.TRAINING_SWORD)) {
        it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation = 567, message = "You already have a training sword.")
        hasSword = true
    }

    if(player.inventory.contains(Items.TRAINING_SHIELD)) {
        it.chatNpc(npc = Npcs.MELEE_COMBAT_TUTOR, title = "Melee combat tutor", animation =  567, message = "You already have a training shield.")
        hasShield = true
    }

    if(!hasSword && !hasShield) {
        it.itemMessageBox(item = Items.TRAINING_SWORD, message = "Harlan gives you a Training sword and shield.")
        player.inventory.add(Items.TRAINING_SWORD,1)
        player.inventory.add(Items.TRAINING_SHIELD, 1)
    }

    if(!hasSword && hasShield) {
        it.itemMessageBox(item = Items.TRAINING_SWORD, message = "Harlan gives you a Training sword.")
        player.inventory.add(Items.TRAINING_SWORD, 1)
    }

    if(hasSword && !hasShield) {
        it.itemMessageBox(item = Items.TRAINING_SHIELD, message = "Harlan gives you a Training shield.")
        player.inventory.add(Items.TRAINING_SHIELD, 1)
    }
}