package gg.rsmod.game.service.serializer.api

import gg.rsmod.game.model.entity.Client
import gg.rsmod.util.ServerProperties
import khttp.responses.Response
import org.json.JSONObject

object Api {
    lateinit var props: ServerProperties

    fun requestToken(username: String, password: String, scopes: String): JSONObject? {
        val url = props.get<String>("oauth_url")
        val client_id = props.get<String>("client_id")
        val client_secret = props.get<String>("client_secret")

        val response: Response = khttp.post(
                url = "$url/oauth/token",
                data = mapOf(
                        "grant_type" to "password",
                        "client_id" to client_id,
                        "client_secret" to client_secret,
                        "username" to username,
                        "password" to password,
                        "scope" to scopes
                )
        )

        if(response.statusCode == 200) {
            return response.jsonObject
        } else {
            return null
        }
    }

    fun refreshToken(client: Client, scopes: String): JSONObject? {
        val url = props.get<String>("oauth_url")
        val client_id = props.get<String>("client_id")
        val client_secret = props.get<String>("client_secret")

        val response: Response = khttp.post(
                url = "$url/oauth/token",
                data = mapOf(
                        "grant_type" to "refresh_token",
                        "refresh_token" to client.refreshToken,
                        "client_id" to client_id,
                        "client_secret" to client_secret,
                        "scope" to scopes
                )
        )

        if(response.statusCode == 200) {
            return response.jsonObject
        } else {
            return null
        }
    }
}