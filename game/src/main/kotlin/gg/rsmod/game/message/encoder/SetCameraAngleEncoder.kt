package gg.rsmod.game.message.encoder

import gg.rsmod.game.message.MessageEncoder
import gg.rsmod.game.message.impl.SetCameraAngleMessage

class SetCameraAngleEncoder : MessageEncoder<SetCameraAngleMessage>() {
    override fun extract(message: SetCameraAngleMessage, key: String): Number = when(key) {
        "localX" -> message.localX
        "localZ" -> message.localZ
        "localY" -> message.localY
        "slowdownSpeed" -> message.slowdownSpeed
        "speed" -> message.speed
        else -> throw Exception("Unhandled key value.")
    }

    override fun extractBytes(message: SetCameraAngleMessage, key: String): ByteArray = throw Exception("Unhandled key value.")
}