package gg.rsmod.game.message.impl

import gg.rsmod.game.message.Message

data class SetCameraPosMessage(val cameraX: Int, val cameraZ: Int, val cameraY: Int, val field4: Int, val field5: Int): Message