package gg.rsmod.game.message.impl

import gg.rsmod.game.message.Message

data class SetCameraAngleMessage(val localX: Int, val localZ: Int, val localY: Int, val slowdownSpeed: Int, val speed: Int) : Message